﻿using System.Threading.Tasks;
using NServiceBus;
using NLog;

namespace NSB_Logging
{
    public class SomeMessageHandler : IHandleMessages<SomeMessage>
    {
        private readonly Logger _logger;

        public SomeMessageHandler(Logger logger)
        {
            _logger = logger;
        }

        public Task Handle(SomeMessage message, IMessageHandlerContext context)
        {
            _logger.Info(message.ToString());
            return Task.CompletedTask;
        }
    }
}
