﻿using Autofac;
using AutofacIdea;

namespace NSB_Logging
{
    public class Bootstrap
    {
        public IContainer Build()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<NLogModule>();

            return builder.Build();
        }
    }
}
