﻿using Autofac;
using NServiceBus;
using NServiceBus.Logging;
using System;
using System.Threading.Tasks;

namespace NSB_Logging
{
    class Program
    {
        public static IContainer _container { get; private set; }

        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            _container = new Bootstrap().Build();

            NLog.Config.ConfigurationItemFactory.Default.CreateInstance = CreateInstance;

            LogManager.Use<NLogFactory>();

            var endpointConfiguration = new EndpointConfiguration("NLogSample");
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.UseTransport<LearningTransport>();
            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.UsePersistence<InMemoryPersistence>();

            endpointConfiguration.UseContainer<AutofacBuilder>(
            customizations: customizations =>
            {
                customizations.ExistingLifetimeScope(_container.BeginLifetimeScope()); // THIS LINE DOESN'T WORK PROPERLY
                //customizations.ExistingLifetimeScope(_container); // THIS LINE WORKS
            });

            var endpoint = await Endpoint.Start(endpointConfiguration);
            try
            {
                await endpoint.SendLocal(new SomeMessage());
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
            finally
            {
                await endpoint.Stop();
            }
        }

        private static object CreateInstance(Type itemType)
        {
            if (_container.IsRegistered(itemType))
            {
                return _container.Resolve(itemType);
            }
            return Activator.CreateInstance(itemType);
        }
    }
}
